namespace :users do
  desc "Upar usuário para admin"
  task :up_to_admin => :environment do
    email = ENV['email']

    u = User.find_by(email: email)
    u.admin = true
    u.save
  end
end