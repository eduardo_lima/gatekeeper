require 'api_constraints'

Gatekeeper::Application.routes.draw do

  # mount SabisuRails::Engine => "/sabisu_rails"
  # devise_for :users
  devise_for :users, skip: :registrations
  # Api definition
  # namespace :api, defaults: { format: :json }, constraints: { subdomain: 'api' }, path: '/'  do
  #   scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
  #     resources :users, :only => [:show, :create, :update, :destroy]
  #     resources :sessions, :only => [:create, :destroy] do
  #       collection do
  #         get    'get_current_user'
  #       end
  #     end

  #     resources :account_activations do
  #       member do
  #         post 'activate' # o id será o token chamadas desta action
  #       end
  #     end

  #     resources :password_recover do
  #       member do
  #         post 'recover' # o id será o token chamadas desta action
  #       end
  #     end

  #   end
  # end
  namespace :api do
    namespace :v1 do
      resources :users, :only => [:show, :create, :update, :destroy]
      resources :sessions, :only => [:create, :destroy] do
        collection do
          get    'get_current_user'
        end
      end

      resources :account_activations do
        member do
          post 'activate' # o id será o token chamadas desta action
        end
      end

      resources :password_recover do
        member do
          post 'recover' # o id será o token chamadas desta action
        end
      end
    end
  end
end


# curl -i -H "Content-Type: application/json" -H "Accept: application/json" -X POST -d '{"user":{"email":"edu.licape@gmail.com", "password": "123123123"}}' http://api.gatekeeper.dev:3000/users

