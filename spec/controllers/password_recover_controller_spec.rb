require 'rails_helper'

RSpec.describe PasswordRecoverController, type: :controller do

  describe "GET #create" do
    it "returns http success" do
      get :create
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #recover" do
    it "returns http success" do
      get :recover
      expect(response).to have_http_status(:success)
    end
  end

end
