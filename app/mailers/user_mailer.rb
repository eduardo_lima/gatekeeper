class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  before_action :set_base_url

  layout 'mailer'

  def set_base_url
    if Rails.env.development? then
      @base_url = "http://localhost:3000/"
    else
      @base_url = "https://furb-gamekeeper.herokuapp.com"
    end
  end

  def account_activation(user)
    @user = user
    mail to: user.email, subject: I18n.t('devise.mailer.confirmation_instructions.subject')
  end

  def password_recover(user)
    @user = user
    mail to: user.email, subject: I18n.t('devise.mailer.reset_password_instructions.subject')
  end
end
