class Api::V1::AccountActivationsController < ApplicationController

  # Ativa a conta cadastrada do usuário.
  # Retorna resposta de usuário ativado ou não.
  def activate
    user = User.find_by(email: params[:email])
    if user && !user.confirmed? && user.authenticated?(:confirmation, params[:id])
      user.confirm
      sign_in user, store: false
      user.generate_authentication_token!
      user.save
      render json: { messages: I18n.t('devise.confirmations.confirm'), status: 200, data: user.to_hash(:basic) }, status: :ok
    else
      render json: { messages: I18n.t('devise.failure.no_token'), status: 401 }, status: :unauthorized
    end
  end

end

