class Api::V1::UsersController < ApplicationController
  before_action :authenticate_with_token!, only: [:show, :update, :destroy]
  respond_to :json

  def show
    render json: { data: User.find(params[:id]).to_hash(:basic) }, status: :ok  
  end

  # Cria a conta de um usuário no gamekeeper e envia email de confirmação.
  # Retorna mensagem de sucesso ou campos com validação.
  def create
    user = User.new(user_params) 
    if user.save
      UserMailer.account_activation(user).deliver_now
      render json: { messages: I18n.t('devise.confirmations.send_instructions'), status: 200 }, status: :ok 
    else
      render json: { messages: user.errors.messages, status: 422 }, status: :unprocessable_entity
    end
  end

  def update
    user = current_user

    if user.update(user_params)
      render json: user, status: 200, location: [:api, user] 
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def destroy
    current_user.destroy
    head 204
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation) 
    end
end
