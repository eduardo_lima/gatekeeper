class Api::V1::PasswordRecoverController < ApplicationController

  def create
    user = User.find_by(email: params[:email].downcase)
    if user
      user.reset_password_token = user.set_reset_password_token
      UserMailer.password_recover(user).deliver_now 
      user.save
      
      render json: { messages: I18n.t('devise.passwords.send_instructions'), status: 200 }, status: :ok 
    else
      render json: { messages: I18n.t('devise.failure.user_not_found'), status: 404 }, status: :not_found
    end
  end

  def recover
    user = User.find_by(email: params[:user][:email])
    user_new_password = params[:user][:password]
    user_new_password_confirmation = params[:user][:password_confirmation]
    
    # reset_password_token
    if user && user.authenticated?(:reset_password, params[:id]) && !user.password_reset_expired? && user.reset_password(user_new_password, user_new_password_confirmation)

      user.clear_reset_password_token

      sign_in user, store: false
      user.generate_authentication_token!
      user.save
    
      render json: { messages: I18n.t('devise.passwords.updated'), status: 200, data: user.to_hash(:basic) }, status: :ok 
    else
      render json: { messages: I18n.t('devise.passwords.not_updated'), status: 422 }, status: :unprocessable_entity
    end
  end

private

  # Obtem quais são os parâmetros pemitidos nas requisições
  # Retorna objeto com parâmetros permitidos.
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end


end
