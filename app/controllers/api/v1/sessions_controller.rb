class Api::V1::SessionsController < ApplicationController
  respond_to :json

  def create 
    user_password = params[:session][:password]
    user_email = params[:session][:email]
    user = user_email.present? && User.find_by(email: user_email) 

    if user && user.confirmed? && user.valid_password?(user_password)
      sign_in user, store: false # isso faz gerar contagem de signins e a data
      user.generate_authentication_token!
      user.save
      render json: { messages: I18n.t('devise.sessions.signed_in'), status: 200, data: user.to_hash(:basic) } #, location: [:api, user]
    else
      render json: { messages: user.blank? ? I18n.t('devise.failure.user_not_found') : 
                               user.confirmed? ? I18n.t('devise.failure.invalid') : I18n.t('devise.failure.unconfirmed'),
                     status: 401 }, status: :unauthorized
    end 
  end

  def get_current_user
    if user_signed_in?
      render json: { messages: I18n.t('devise.sessions.signed_in'), status: 200, data: @current_user.to_hash(:basic) }, status: :ok
    else
      render json: { messages: I18n.t('devise.sessions.not_signed_in'), status: 401 }, status: :unauthorized
    end
  end

  def destroy
    user = User.find_by(auth_token: params[:id])    
    user.generate_authentication_token! # tornar o token inútil
    user.save
    @current_user = nil

    render json: { messages: I18n.t('devise.sessions.signed_out'), status: 200 }, status: :ok
  end
end
