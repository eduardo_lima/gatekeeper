module Authenticable
  
  # Devise methods overwrites
  def current_user
    @current_user ||= User.find_by(auth_token: request.headers['Authorization']) 
  end

  def authenticate_with_token!
    render json: { messages: I18n.t('devise.errors.messages.expired'), status: 401 }, status: :unauthorized unless user_signed_in?
  end

  def user_signed_in?
    current_user.present? #&& !current_user.timedout?(3.minutes.ago)
  end
  
end
