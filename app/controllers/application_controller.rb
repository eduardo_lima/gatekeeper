class ApplicationController < ActionController::Base
  include Authenticable

  before_action :set_locale

  respond_to :json

  def set_locale
    # I18n.locale = params[:locale] || I18n.default_locale
    I18n.locale = :pt_BR
  end

end
