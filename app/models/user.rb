class User < ActiveRecord::Base
  validates :auth_token, uniqueness: true

  devise :database_authenticatable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :timeoutable

         # ver timeoutable!!!

  before_create :generate_authentication_token!

  # Converte os campos do model de acordo com a opcao determinada.
  # Retorna um hash com os campos do model.
  #
  # O parametro 'inc' define qual serao os campos incluidos no 
  # hash. Quando 'inc' for <tt>:all</tt> todos os campos devem 
  # estar no hash. Quando 'inc' for <tt>:basic</tt> os campos de id,
  # email e nome devem estar no hash.
  def to_hash(inc=:all)
    inc = Array(inc)
    hash = {}
    
    if inc.include?(:all) or inc.include?(:basic) then
      hash[:id] = self.id
      hash[:email] = self.email
      hash[:name] = self.name
      hash[:auth_token] = self.auth_token
      hash[:admin] = self.admin
    end
    
    return hash
  end

  def generate_authentication_token!
    begin
      self.auth_token = Devise.friendly_token
    end while self.class.exists?(auth_token: auth_token)
  end

  def set_reset_password_token
    super
  end

  def reset_password(new_password, new_password_confirmation)
    super(new_password, new_password_confirmation)
  end

  def clear_reset_password_token
    super
  end

  def remember_me!
    self.remember_token = Devise.friendly_token
    super
  end

  def authenticated?(attr, token)
    self.send("#{attr}_token") == token
  end

  def password_reset_expired? 
    self.reset_password_sent_at < 2.hours.ago
  end
end
